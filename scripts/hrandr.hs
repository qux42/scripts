import Data.Char (isDigit, isSpace)
import Data.Maybe (fromJust, isJust)
import Data.List (find, intersect)
import System.Process (readProcess)
import Text.Regex.Posix ((=~))
import System.Cmd (rawSystem)
import System.Environment (getArgs)


data ScreenInfo= ScreenInfo{
      name::String
    , connected::Bool
    --nicht vorhanden falls Bildschirm nicht genutzt wird
    , currentResolution::Maybe Resolution 
    , possibleResolutions::[Resolution]
  }
  deriving Show
type Resolution= (Int, Int)
type XrandrBlock= [String]

data Direction = LeftOf| Above | RightOf | Below | SameAs | Internal | External
        deriving Eq

getDirection :: [String] -> Direction
getDirection [] = Internal
getDirection (x:_)
        | x == "LeftOf"   = LeftOf
        | x == "Above"    = Above
        | x == "RightOf"  = RightOf
        | x == "Below"    = Below
        | x == "SameAs"   = SameAs
        | x == "External" = External
        | otherwise       = Internal

instance Show Direction where
   show LeftOf  = "--left-of"
   show Above   = "--above"
   show RightOf = "--right-of"
   show Below   = "--below"
   show SameAs  = "--same-as"

getXrandrArgs :: ScreenInfo -> Direction -> Maybe ScreenInfo-> [ScreenInfo] ->[String]
getXrandrArgs iS d eS rest
      | isJust eS && d==External = 
                [ "--output", name iS
                , "--off"
                , "--output", name (fromJust eS)
                , "--mode",   resolution2String$head$possibleResolutions$fromJust eS
                ] ++ turnInactiveScreensOff rest
      | isJust eS && d==Internal = 
                [ "--output", name iS
                , "--mode",   resolution2String maxResIS
                , "--output", name (fromJust eS)
                , "--off"
                ] ++ turnInactiveScreensOff rest
      | isJust eS  = 
                [ "--output", name iS
                , "--mode",   resolution2String maxResIS
                , "--output", name (fromJust eS)
                , "--mode",   resolution2String maxResES
                , show d ,    name iS] ++ turnInactiveScreensOff rest
      | otherwise       =
                [ "--output", name iS
                , "--mode", resolution2String$head$possibleResolutions iS
                ] ++ turnInactiveScreensOff rest
   where (maxResIS, maxResES)  = getMaxRes iS  (fromJust eS)  d
        
        


getMaxRes :: ScreenInfo -> ScreenInfo -> Direction -> (Resolution, Resolution)
getMaxRes i e SameAs =  (max, max)
        where max =  head $ intersect (possibleResolutions i) (possibleResolutions e)
getMaxRes i e _ = (head (possibleResolutions i), head (possibleResolutions e))






resolution2String :: Resolution -> String
resolution2String (xResolution, yResolution) = show xResolution ++"x"++ show yResolution

--gibt Bloecke mit Name und Aufloesungen zurueck
getXrandrBlocks :: [String]->[XrandrBlock]
getXrandrBlocks []     = []
getXrandrBlocks (x:xs) = (x:posResolutions):getXrandrBlocks rest
        where (posResolutions,rest) = break ((' '/=).head) xs

xrandrBlockToScreenInfo :: XrandrBlock->ScreenInfo
xrandrBlockToScreenInfo (x:xs)= ScreenInfo name connected currentResolution possibleResolutions 
         where (name:c:cr:_)= words x
               connected= c=="connected"
               currentResolution= getCurrentResolution cr
               possibleResolutions= getPossibleResolutions xs  

getCurrentResolution :: String -> Maybe Resolution
getCurrentResolution x 
        | isDigit (head x) = Just (read xResolution, read yResolution)
        | otherwise= Nothing
        where (xResolution, rest)= break (=='x') x
              yResolution= takeWhile isDigit (tail rest)
                
getPossibleResolutions :: [String] -> [Resolution]
getPossibleResolutions = map (fromJust . getCurrentResolution . dropWhile isSpace)    

getInternalScreen :: [ScreenInfo] -> ScreenInfo
getInternalScreen =  fromJust.find ((=~"LVDS").name )

getExternalScreens :: [ScreenInfo] -> [ScreenInfo]
getExternalScreens = filter (\x -> not (name x=~"LVDS"))  


getActiveScreen :: [ScreenInfo] -> Maybe ScreenInfo
getActiveScreen = find connected



turnInactiveScreensOff :: [ScreenInfo] -> [String]
turnInactiveScreensOff = concatMap (\ x -> "--output":name x:["--off"]).filter (not.connected) 

--  xrandr --output LVDS1 --mode 1024x768 --output VGA1 --mode 1024x768 --same-as LVDS1
main = do
    xrandrOutput <- readProcess "xrandr" [] []
    let xrandrBlocks = getXrandrBlocks $ tail $ lines xrandrOutput
    let screenInfos = map xrandrBlockToScreenInfo xrandrBlocks
   -- print screenInfos
    let internalScreen   = getInternalScreen  screenInfos
    let externalScreens  = getExternalScreens screenInfos
   -- print internalScreen
   -- print externalScreen
    let d = External
    args <- getArgs
    let d = getDirection args
    rawSystem "xrandr" (getXrandrArgs internalScreen d (find connected externalScreens) externalScreens)
    {-print (turnInactiveScreensOff externalScreens)-}
--  xrandr --output LVDS1 --mode 1024x768 --output VGA1 --mode 1024x768 --same-as LVDS1



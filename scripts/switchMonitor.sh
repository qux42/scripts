#!/bin/zsh
file=~/.monitorStatus
if [ -e $file ] ; then
else 
	echo 0 > $file
fi
a=$(cat $file)
if [ -z "$a" ] ; then
	a=0
fi
if [ $a = 0 ] ; then
	hrandr SameAs
elif [ $a = 1 ] ; then
	hrandr RightOf
elif [ $a = 2 ] ; then
	hrandr External
elif [ $a = 3 ] ; then
    hrandr Internal
fi
a=$(( 10#$a +1 ))
if [ $a = 4 ] ; then
	a=0
fi
echo $a > $file
# trayer neustarten
killall trayer
trayer --edge top --align right --SetDockType true --SetPartialStrut true \
 --expand true --width 10 --transparent true --tint 0x000000 --height 12 &
# hintergrund neu setzen
feh --bg-max /home/angizia/.wallpaper/Firefox_wallpaper.png


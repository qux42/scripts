#!/bin/bash

getState(){
	mpc status | egrep -o -m 1 -e '\[playing\]|\[paused\]' 
}
getCurrent(){
	mpc current 
}

current=$(getCurrent)
state=$(getState)t
if [ $state = \[playing\]t ] ; then
	echo "<fc=green>$current</fc>"
elif [ $state = \[paused\]t ] ; then
	echo "<fc=blue>$current</fc>"
else
	echo
fi

counter=0
while  [ -x /usr/bin/inotifywait ] ; do
	if [ $counter = 0 ] ; then
		xautolock -enable
	fi
	sleep .01
	event=$(inotifywait /usr/bin/mplayer -e OPEN -e CLOSE --format %e)
	if [ $event = OPEN ] ; then
		counter=$(( $counter+1 ))
		xautolock -disable
	else
		counter=$(( $counter-1 ))
	fi
done

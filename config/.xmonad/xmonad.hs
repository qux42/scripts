-- Imports.
import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Layout
import XMonad.Layout.NoBorders
import XMonad.Hooks.ManageDocks
import Data.Ratio ((%))
import Data.Map    (fromList)
import Data.Monoid (mappend)
import XMonad.Util.EZConfig
import XMonad.Actions.CycleWindows
import XMonad.Actions.CycleWS 
import XMonad.Hooks.EwmhDesktops
import XMonad.Actions.GridSelect
import XMonad.Layout.ThreeColumns
import XMonad.Hooks.SetWMName




-- The main function.
main = xmonad =<< statusBar myBar myPP toggleStrutsKey ( ewmh myConfig)

myManageHook :: ManageHook
myManageHook = composeAll [
    className =? "Xfce4-notifyd" -->doIgnore
    ]
-- Command to launch the bar.
myBar = "xmobar"

layout' = avoidStruts (smartBorders (tiled ||| Mirror tiled ||| Full))
          where
                tiledMid = ThreeColMid nmaster delta ratio
                tiled = Tall nmaster delta ratio
                {-tiled   = Tall nmaster delta ratio-}
                nmaster = 1     -- The default number of windows in the master pane
                ratio   = 2%3   -- Default proportion of screen occupied by master pane
                delta   = 5%100 -- Percent of screen to increment by when resizing panes
     


-- Custom PP, configure it as you like. It determines what is being written to the bar.
myPP = xmobarPP { ppCurrent = xmobarColor "#429942" "" . wrap "<" ">" }

-- Key binding to toggle the gap for the bar.
toggleStrutsKey XConfig {XMonad.modMask = modMask} = (modMask, xK_b)

-- Main configuration, override the defaults to your liking. 
myConfig = defaultConfig 
        { terminal = "urxvtc" 
  --      , logHook = ewmhDesktopsStartup >> setWMName "LG3D"
        , modMask  = mod4Mask 
        , borderWidth = 2  
        , focusedBorderColor= "#ff00ff"
        , normalBorderColor= "#000000"
        , manageHook  = myManageHook
        , layoutHook  = layout'
        , handleEventHook    = fullscreenEventHook
 --       , startupHook = ewmhDesktopsStartup >> setWMName "LG3D"
        {-, startupHook =  setWMName "LG3D" <+> ewmhDesktopsStartup-}
        , keys = keys defaultConfig `mappend`
              \c -> fromList [
                ((0, 0x1008FF13), spawn "amixer set Master 1%+ unmute")
                ,((0, 0x1008FF12), spawn "amixer set Master togglemute")
                ,((0, 0x1008FF11), spawn "amixer set Master 1%- unmute")
                --Fn+F2: lockscreen
                ,((0, 0x1008FF2D), spawn "xautolock -locknow")
                --Fn+F3: suspend
--                ,((0, 0x1008FF93), spawn "suspend.sh")
                --Fn+F12: hibernate
                ,((0, 0x1008FFA7), spawn "hibernate.sh")
                ,((0, 0x1008FF41), spawn "urxvtc -e ncmpcpp")
                --Fn+F8: toggle Touchpad
                ,((0, 0x1008FFA9), spawn "touchpad-toggle.sh")   
                --for screenshots
                ,((0, 0xFF61    ), spawn "import -window root ~/bilder/screenshot_`date '+%Y%m%d-%H%M%S'`.png")
                ,((0, 0x1008FF59), spawn "switchMonitor.sh") 
                --,((0, 0x1008FF02), spawn "xbacklight +10")
                -- ,((0, 0x1008FF03), spawn "xbacklight -10")
          ]
        } `additionalKeysP` myKeysP


myKeysP = [ ("M-f",   spawn "urxvtc -e vifm")
           ,("M-c",   spawn "chromium")
 --          ,("M-k",  swapNextScreen)
           ,("M-p", spawn "exe=`/home/angizia/.cabal/bin/yeganesh -x` && eval \"exec $exe\"" )
           ,("M1-<Tab>", cycleRecentWindows [xK_Alt_L] xK_Tab xK_Tab )
           ,("M-<Tab>", toggleWS)
           ,("M-<Right>", nextWS)
           ,("M-<Left>", prevWS)
           ,("M-S-<Right>", shiftToNext)
           ,("M-S-<Left>", shiftToPrev)
           ,("<XF86AudioPlay>", spawn "mpc toggle")   
           ,("<XF86AudioNext>", spawn "mpc next")   
           ,("<XF86AudioPrev>", spawn "mpc prev")   
           ,("<XF86AudioStop>", spawn "mpc stop")   
--          , ("<XF86Launch1>",           spawn "brightness toggle")
                     ]


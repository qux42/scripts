call pathogen#infect()
let mapleader = ","
set nocompatible
syn on
set showcmd
set mouse=a
filetype plugin on
filetype plugin indent on
set number
"set spell
set spelllang=en,de
set foldenable
set foldmethod=syntax

"for nerdtree
map <Leader>nt :NERDTree<CR>
"for tagbar
map <Leader>tb :TagbarToggle<CR>
"for status-bar 
set statusline=%F%m%r%h%w\ [TYPE=%Y]\ [%p%%]\ [POS=%04l,%04v][LEN=%L]
set laststatus=2


"for cursor-tracking
set cursorcolumn
hi cursorcolumn cterm=NONE ctermbg=white
hi CursorLine   cterm=NONE ctermbg=Magenta
"set cursor-line

"ctrl+n for next tab
map <C-n> :tabnext<CR>



" map <ctrl>+F12 to generate ctags for current folder:
" build tags of your own project with CTRL+F12     
map <C-F12> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR>     
"noremap <F12> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<cr>     
"inoremap <F12> <Esc>:!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<cr>    
" auto close options when exiting insert mode
autocmd InsertLeave * if pumvisible() == 0|pclose|endif
" -- configs --
let OmniCpp_MayCompleteDot = 1 " autocomplete with .
let OmniCpp_MayCompleteArrow = 1 " autocomplete with ->
let OmniCpp_MayCompleteScope = 1 " autocomplete with ::
let OmniCpp_SelectFirstItem = 2 " select first item (but don't insert)
let OmniCpp_NamespaceSearch = 1 " search namespaces in this and included files
let OmniCpp_GlobalScopeSearch = 1 
let OmniCpp_ShowPrototypeInAbbr = 1 " show function prototype (i.e. parameters) in popup window

"set tags=/home/angizia/.cTags/std3.3.tags
"set tags+=~/.cTags/scala.tags
set tags=./tags;/,$HOME/.cTags/std3.3.tags,$HOME/.cTags/vtk.tags

set tabstop=4
set sw=4


"haskell
let g:haddock_browser = 'firefox'
au Bufenter *.hs compiler ghc
autocmd Filetype haskell setlocal ts=8 sw=4 sts=0 expandtab

"latex
filetype plugin on
filetype indent on
au BufEnter *.tex set autowrite
let g:Tex_DefaultTargetFormat = 'pdf'
let g:Tex_MultipleCompileFormats = 'pdf'
let g:Tex_CompileRule_pdf = 'makeindex $*.nlo -s nomencl.ist -o $*.nls & pdflatex -interaction=nonstopmode -synctex=1 $*'
let g:Tex_SmartKeyQuote=0

"enable copy by strg+c
map <C-c> "+y<CR> 

",cd set working directory to the current file
nnoremap ,cd :cd %:p:h<CR>:pwd<CR>

set backspace=2 " make backspace work like most other apps
command W w !sudo tee % >/dev/null

set background=dark

colorscheme bandit 
set hlsearch
"strg+f im insert macht klammer zu"
imap <C-F> {<CR>}<C-O>O
hi Normal ctermbg=None 
"wrap left and right automatically
set whichwrap+=<,>,h,l,[,]

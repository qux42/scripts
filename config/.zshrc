# This file is sourced only for interactive shells. It
# should contain commands to set up aliases, functions,
# options, key bindings, etc.
#
# Global Order: zshenv, zprofile, zshrc, zlogin

#export TERM=xterm-256color
#alias tmux='TERM=xterm-256color tmux'
#[[ $TMUX != *tmux-100* ]] && exec tmux

# ????
fpath=(~/.zsh $fpath)
export PATH="/home/angizia/.cabal/bin":$PATH
#export TERM=xterm-256color

READNULLCMD=${PAGER:-/usr/bin/pager}
eval "`dircolors`"

#ibus?
#export GTK_IM_MODULE=ibus
#export XMODIFIERS=@im=ibus
#export QT_IM_MODULE=ibus

# for home/end and so on
if [[ "$TERM" != emacs ]]; then
	bindkey -e
	[[ -z "$terminfo[kdch1]" ]] || bindkey -M emacs "$terminfo[kdch1]" delete-char
	[[ -z "$terminfo[khome]" ]] || bindkey -M emacs "$terminfo[khome]" beginning-of-line
	[[ -z "$terminfo[kend]" ]] || bindkey -M emacs "$terminfo[kend]" end-of-line
	# bind [CTRL]+[LeftArrow] and [CTRL]+[RightArrow]
	# the commands can be found at man zshzle
	# the array indices for terminfo are found in man 5 terminfo
	# for keys which arent listed in the manpage press [CTRL]+[V]<key> to get its code
	bindkey -M emacs '^[[1;5D' backward-word
	bindkey -M emacs '^[[1;5C' forward-word
fi

# do a cd if the written text in the cli is not a command but is a dir
setopt AUTO_CD AUTO_PUSHD PUSHD_IGNORE_DUPS
###############################################
# history 
########
# dont put cli lines beginnign with space into command history
setopt hist_ignore_space    

# If a new command line being added to the history list duplicates an older one, the older command is removed from the list
setopt hist_ignore_all_dups 

# not just at the end  
setopt complete_in_word

setopt append_history
setopt share_history

export HISTFILE=~/.history
export HISTSIZE=10000
export SAVEHIST=10000





# print a error message when commands like the following dosent match any files: echo hund*
setopt no_match             
  setopt bad_pattern          # print a error message when in case of errorous patterns like: echo [-
unsetopt sh_word_split        # X='hund katze maus' echo $X     <-- the echo command would get 3 args if this were on
unsetopt glob_subst           # X='*' echo $x                   <-- i would see all files if thise were on
unsetopt ksh_glob             # i dont do korn shell
unsetopt ksh_arrays           # i dont do korn shell
unsetopt ksh_autoload         # i dont do korn shell
unsetopt prompt_percent       # some historic korn shell compatibility stuff, i dont do korn shell
  setopt prompt_subst         # enable fancy substitution syntax in shell prompts
  setopt no_check_jobs        # don't warn me about background processes when exiting
  setopt no_hup               # don't kill background processes when exiting
unsetopt rm_star_silent       # warn me when i do rm *
unsetopt bg_nice              # dont lower the priority of processes i send to the background
unsetopt notify               # dont spam console when backgrounded processes finish, show the message on return or ctrl-c
unsetopt correct              # dont enable command correction
unsetopt globdots             # rm * should not remove .somefile
unsetopt csh_junkie_history   # i dont do c shell
unsetopt csh_junkie_loops     # i dont do c shell
unsetopt csh_null_glob        # i dont do c shell
unsetopt csh_junkie_quotes    # i dont do c shell
unsetopt extended_glob        # dont do too fancy globbing, i wont use it anyways

alias reboot='systemctl reboot'
alias halt='systemctl poweroff'

alias mplayer='mplayer -vo vaapi'
alias ls='ls --color=auto'
alias files='ls | wc -l'
alias grep='grep --color=auto'
alias rmr='rm -R'
alias rmrf='rm -Rf'
alias less='less -S -M'
alias mountl='mount -l | column -t'
alias df='df -h'
alias du='du -c -h'
alias du1='du  --max-depth=1'
alias diff='colordiff'
alias mkdir='mkdir -p -v'
alias pg='ps -Af | grep $1'
# safety features
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -I'                    # 'rm -i' prompts for every file
alias ln='ln -i'
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'
alias feh='feh --auto-zoom --scale-down --geometry 1360x1024 --magick-timeout 2'


if (which ls++ > /dev/null) ; then
	alias ll='ls++ -l'
	alias la='ls++ -la'
	alias llh='ls++ -lh'
	alias lah='ls++ -lah'
else
	alias ll='ls -l --color=auto'
	alias la='ls -la --color=auto'
	alias llh='ls -lh --color=auto'
	alias lah='ls -lah --color=auto'
fi

if (which most > /dev/null) ; then
	export PAGER='most'
	alias mountlm='mount -l | column -t | most'
else
	export PAGER='less'
fi

export EDITOR='vim'
export VISUAL='vim'
export BROWSER='firefox'

EXPECTED_M2_HOME='/usr/local/apache-maven-2.2.1'
EXPECTED_M2="${EXPECTED_M2_HOME}/bin"
EXPECTED_JAVA_HOME='/usr/lib/jvm/java-6-sun/'
if [ -d "${EXPECTED_M2_HOME}" -a -d "${EXPECTED_M2}" -a -d "${EXPECTED_JAVA_HOME}" ] ; then
	export M2_HOME="${EXPECTED_M2_HOME}"
	export M2="${EXPECTED_M2}"
	export JAVA_HOME="${EXPECTED_JAVA_HOME}"
	export PATH="$PATH:$M2:$JAVA_HOME/bin"
fi

EXPECTED_ECLIPSE_PATH='/usr/local/eclipse/'
if [ -d "${EXPECTED_ECLIPSE_PATH}" ] ; then
	export PATH="$PATH:${EXPECTED_ECLIPSE_PATH}"
fi

for EXPECTED_GIT_ACHIEVEMENTS_PATH in "${HOME}/vcs/git/git-achievements" '/usr/local/src/git-achievements' '/usr/local/git-achievements' ; do
	if ! alias  |  grep -q git-achievements ; then
		EXPECTED_GIT_ACHIEVEMENTS_FILE="${EXPECTED_GIT_ACHIEVEMENTS_PATH}/git-achievements"
		if [ -x "${EXPECTED_GIT_ACHIEVEMENTS_FILE}" ] ; then
			export PATH="$PATH:${EXPECTED_GIT_ACHIEVEMENTS_PATH}"
			alias git='git-achievements'
		fi
	fi
done

zstyle ':completion:*:sudo:*' command-path  /usr/local/sbin /usr/local/bin /usr/sbin /usr/bin /sbin /bin /usr/X11R6/bin
zstyle ':completion:*'        list-colors   ${(s.:.)LS_COLORS}
zstyle ':completion:*'        list-prompt   %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*'        menu          select=long-list select=0
zstyle ':completion:*'        select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' cache-path ~/.zsh/cache
zstyle ':completion:*' use-cache on

zmodload zsh/complist
bindkey -M menuselect '^o' accept-and-infer-next-history
bindkey -M menuselect '^M' .accept-line


autoload -U compinit
compinit

autoload -U promptinit && promptinit
if prompt -l | grep -q sseering ; then
	prompt sseering
fi

# make stderr red
# STDERRED_LIB='/usr/lib/stderred.so'
# if [ -f "${STDERRED_LIB}" ] ; then
# 	export LD_PRELOAD="${STDERRED_LIB}"
# fi

# expand-or-complete-or-list-files a.k.a. map tab on an empty line to ls
#function expand-or-complete-or-list-files() {
#	if [[ $#BUFFER == 0 ]]; then
#		BUFFER="ls "
#		CURSOR=3
#		zle list-choices
#		zle backward-kill-word
#	else
#		zle expand-or-complete
#	fi
#}
#zle -N expand-or-complete-or-list-files
#bindkey '^I' expand-or-complete-or-list-files
#
#

#music and video
alias -s flv=mplayer-resumer
alias -s mkv=mplayer-resumer
alias -s mp3=mplayer
alias -s m4a=mplayer
alias -s avi=mplayer
alias -s pdf=mupdf
#pictures
alias -s jpg=feh
alias -s png=feh
#textfiles
alias -s tex=vim
alias -s txt=vim
#archives
alias -s tar='tar xf'
alias -s bz='tar xf'
alias -s gz='tar xf'
alias -s tgz='tar xf'
alias -s bz2='tar xjvf'
alias -s rar='unrar x'
alias -s zip='7z x'
alias -s 7z='7z x'

man() {
	env \
		LESS_TERMCAP_mb=$(printf "\e[1;31m") \
		LESS_TERMCAP_md=$(printf "\e[1;31m") \
		LESS_TERMCAP_me=$(printf "\e[0m") \
		LESS_TERMCAP_se=$(printf "\e[0m") \
		LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
		LESS_TERMCAP_ue=$(printf "\e[0m") \
		LESS_TERMCAP_us=$(printf "\e[1;32m") \
			man "$@"
}
